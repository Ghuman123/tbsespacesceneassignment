﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TBSESpaceSceneGenerator;

namespace TBSESpaceSceneGeneratorTests
{
    [TestClass]
    public class RandomSingletonTests
    {
        [TestMethod]
        public void ReturnInstanceTest()
        {
            RandomSingleton random = RandomSingleton.Instance();
            Assert.IsNotNull(random);
        }

        [TestMethod]
        public void SeedNotSet()
        {
            RandomSingleton random = RandomSingleton.Instance();

            try
            {
                random.Next();
            }
            catch (Exception e)
            {
                Assert.IsInstanceOfType(e, typeof(SeedNotSetException));
            }
        }

        [TestMethod]
        public void NextInt()
        {
            RandomSingleton random = RandomSingleton.Instance();
            random.SetSeed(0);
            int value = random.Next();
            Assert.IsTrue(value < Int32.MaxValue);
            Assert.IsTrue(value >= Int32.MinValue);
        }

        [TestMethod]
        public void NextIntMax()
        {
            RandomSingleton random = RandomSingleton.Instance();
            random.SetSeed(0);
            int value = random.Next(2);
            Assert.IsTrue(value < 2);
            Assert.IsTrue(value >= Int32.MinValue);
        }

        [TestMethod]
        public void NextIntMinMax()
        {
            RandomSingleton random = RandomSingleton.Instance();
            random.SetSeed(0);
            int value = random.Next(1, 2);
            Assert.IsTrue(value < 2);
            Assert.IsTrue(value >= 1);
        }

        [TestMethod]
        public void NextByte()
        {
            RandomSingleton random = RandomSingleton.Instance();
            random.SetSeed(0);
            byte[] buffer = new byte[100];
            random.NextByte(buffer);
            Assert.IsNotNull(buffer);
            Assert.AreNotEqual(new byte[100], buffer);
        }

        [TestMethod]
        public void NextDouble()
        {
            RandomSingleton random = RandomSingleton.Instance();
            random.SetSeed(0);
            double value = random.NextDouble();
            Assert.IsTrue(value < 1.0);
            Assert.IsTrue(value >= 0.0);
        }

        [TestMethod]
        public void NextDoubleMax()
        {
            RandomSingleton random = RandomSingleton.Instance();
            random.SetSeed(0);
            double value = random.NextDouble(5.0);
            Assert.IsTrue(value < 5.0);
            Assert.IsTrue(value >= 0.0);
        }

        [TestMethod]
        public void NextDoubleMinMax()
        {
            RandomSingleton random = RandomSingleton.Instance();
            random.SetSeed(0);
            double value = random.NextDouble(1.0, 2.0);
            Assert.IsTrue(value < 2.0);
            Assert.IsTrue(value >= 1.0);
        }
    }
}
