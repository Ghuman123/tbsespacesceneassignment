﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TBSESpaceSceneGenerator.Structures
{
    public enum TerrainFeature
    {
        Ocean,
        Desert,
        Forest,
        Moutain,
        OtherLand,
        Count
    }

    class Terrain
    {
        public Dictionary<TerrainFeature, double> Features { get; private set; }

        public Terrain()
        {
            Features = new Dictionary<TerrainFeature, double>();
        }

        public void AddFeature(TerrainFeature feature, double percentage)
        {
            if (Features.ContainsKey(feature))
                Features[feature] = percentage;
            else
                Features.Add(feature, percentage);
        }

        public double GetFeaturePercentage()
        {
            double sum = 0.0;
            foreach (KeyValuePair<TerrainFeature, double> kvp in Features)
            {
                sum += kvp.Value;
            }
            return sum;
        }
    }
}
