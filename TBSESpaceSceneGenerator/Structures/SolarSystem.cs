﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TBSESpaceSceneGenerator.Structures
{
    class SolarSystem
    {
        private static int Count = 0;

        public int ID { get; private set; }
        public Star Star { get; set; }
        public AsteroidBelt AsteroidBelt { get; set; }
        public ConcurrentBag<Planet> Planets { get; private set; }
        public ConcurrentBag<Comet> Comets { get; private set; }
        private Object thisLock = new Object();

        public SolarSystem(int ID)
        {
            //lock (thisLock)
            //{
            //    this.ID = Count;
            //    Interlocked.Add(ref Count, 1);
            //}
            this.ID = ID;
            Star = null;
            AsteroidBelt = null;
            Planets = new ConcurrentBag<Planet>();
            Comets = new ConcurrentBag<Comet>();
        }

        public static void ResetCount()
        {
            Count = 0;
        }

        public void AddPlanet(Planet planet)
        {
            Planets.Add(planet);
        }

        public void AddPlanets(ConcurrentBag<Planet> planets)
        {
            foreach (Planet p in planets)
            {
                Planets.Add(p);
            }
        }

        public void AddComet(Comet comet)
        {
            Comets.Add(comet);
        }

        public void AddComets(ConcurrentBag<Comet> comets)
        {
            foreach (Comet c in comets)
            {
                Comets.Add(c);
            }
        }
    }
}
