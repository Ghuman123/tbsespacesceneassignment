﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TBSESpaceSceneGenerator.Structures
{
    enum OrbitType
    {
        Geosynchronous,
        Geostationary,
        SemiSynchronous,
        Geocentric,
        GraveyardOrbit,
        Count
    }

    enum SatelliteType
    {
        Station,
        TV,
        Radio,
        Internet,
        Communication,
        Weather,
        Military,
        Count
    }

    class Satellite
    {
        public bool IsDockable { get; set; }

        public bool IsHabitable { get; set; }

        public String CountryOfOrigin { get; set; }

        public OrbitType Orbit { get; set; }

        public SatelliteType Type { get; set; }
    }
}
