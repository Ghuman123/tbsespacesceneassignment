﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using TBSESpaceSceneGenerator.Structures;

namespace TBSESpaceSceneGenerator.XML
{
    static class SceneWriter
    {
        private static void CreateSceneFolder()
        {
            if (!Directory.Exists("Scene"))
            {
                Directory.CreateDirectory("Scene");
            }
        }

        public static void WriteSolarSystemFiles(ConcurrentQueue<SolarSystem> solarSystems)
        {
            List<Task> Tasks = new List<Task>();
            CreateSceneFolder();

            foreach (SolarSystem s in solarSystems)
            {
                Task T = Task.Factory.StartNew(() =>
                    {
                        XmlTextWriter writer = new XmlTextWriter("Scene/SolarSystem" + s.ID + ".xml", Encoding.Unicode);

                        writer.Formatting = Formatting.Indented;
                        writer.Indentation = 4;

                        writer.WriteStartDocument();
                        writer.WriteStartElement("SolarSystem");

                        writer.WriteAttributeString("ID", s.ID.ToString());

                        WriteStar(writer, s.Star);

                        WritePlanets(writer, s.Planets);

                        WriteComets(writer, s.Comets);

                        if (s.AsteroidBelt != null)
                            WriteAsteroidBelt(writer, s.AsteroidBelt);

                        writer.WriteEndElement();
                        writer.WriteEndDocument();
                        writer.Close();
                        
                        
                    });
                Tasks.Add(T);
            }
            Task.WaitAll(Tasks.ToArray());
        }

        private static void WriteStar(XmlTextWriter writer, Star star)
        {
            writer.WriteStartElement("Star");
            writer.WriteAttributeString("Class", Enum.GetName(typeof(StarClass), star.Class));
            writer.WriteElementString("Mass", star.Mass.ToString());
            writer.WriteElementString("Radius", star.Radius.ToString());
            writer.WriteElementString("Temperature", star.Temperature.ToString());
            writer.WriteElementString("Color", star.ColourDescription);
            writer.WriteEndElement();
        }

        private static void WritePlanets(XmlTextWriter writer, ConcurrentBag<Planet> planets)
        {
            writer.WriteStartElement("Planets");

            foreach (Planet p in planets)
            {
                writer.WriteStartElement("Planet");
                writer.WriteAttributeString("Type", Enum.GetName(typeof(PlanetType), p.Type));
                writer.WriteElementString("IsHabitable", p.IsHabitable.ToString());
                writer.WriteElementString("Diameter", p.Diameter.ToString());
                writer.WriteElementString("Mass", p.Mass.ToString());
                writer.WriteElementString("SemiMajorAxis", p.SemiMajorAxis.ToString());
                writer.WriteElementString("OrbitalPeriod", p.OrbitalPeriod.ToString());
                writer.WriteElementString("OrbitalEccentricity", p.OrbitalEccentricity.ToString());
                writer.WriteElementString("RotationPeriod", p.RotationPeriod.ToString());
                WriteMoons(writer, p.Moons);
                WriteSatellites(writer, p.Satellites);
                if (p.Atmosphere != null)
                    WriteAtmosphere(writer, p.Atmosphere);
                if (p.Ring != null)
                    WriteRing(writer, p.Ring);
                if (p.Terrain != null)
                    WriteTerrain(writer, p.Terrain);
                writer.WriteEndElement();
            }

            writer.WriteEndElement();
        }

        private static void WriteMoons(XmlTextWriter writer, ConcurrentBag<Moon> moons)
        {
            writer.WriteStartElement("Moons");
            foreach (Moon m in moons)
            {
                writer.WriteStartElement("Comet");
                writer.WriteElementString("Diameter", m.Diameter.ToString());
                writer.WriteElementString("Mass", m.Mass.ToString());
                writer.WriteEndElement();
            }
            writer.WriteEndElement();
        }

        private static void WriteSatellites(XmlTextWriter writer, ConcurrentBag<Satellite> satellites)
        {
            writer.WriteStartElement("Satellites");
            foreach (Satellite s in satellites)
            {
                writer.WriteStartElement("Satellite");
                writer.WriteElementString("IsDockable", s.IsDockable.ToString());
                writer.WriteElementString("IsHabitable", s.IsHabitable.ToString());
                writer.WriteElementString("CountryOfOrigin", s.CountryOfOrigin);
                writer.WriteElementString("Orbit", Enum.GetName(typeof(OrbitType), s.Orbit));
                writer.WriteElementString("Type", Enum.GetName(typeof(SatelliteType), s.Type));
                writer.WriteEndElement();
            }
            writer.WriteEndElement();
        }

        private static void WriteAtmosphere(XmlTextWriter writer, Atmosphere atmosphere)
        {
            writer.WriteStartElement("Atmosphere");
            foreach (KeyValuePair<Gas, double> g in atmosphere.Gases)
            {
                writer.WriteStartElement("Gas");
                writer.WriteAttributeString("Type", Enum.GetName(typeof(Gas), g.Key));
                writer.WriteAttributeString("Percentage", g.Value.ToString());
                writer.WriteEndElement();
            }
            writer.WriteEndElement();
        }

        private static void WriteRing(XmlTextWriter writer, PlanetaryRing ring)
        {
            writer.WriteStartElement("PlanetaryRing");
            writer.WriteAttributeString("Composition", Enum.GetName(typeof(RingComposition), ring.Composition));
            writer.WriteEndElement();
        }

        private static void WriteTerrain(XmlTextWriter writer, Terrain terrain)
        {
            writer.WriteStartElement("Terrain");
            foreach (KeyValuePair<TerrainFeature, double> f in terrain.Features)
            {
                writer.WriteStartElement("Feature");
                writer.WriteAttributeString("Type", Enum.GetName(typeof(TerrainFeature), f.Key));
                writer.WriteAttributeString("Percentage", f.Value.ToString());
                writer.WriteEndElement();
            }
            writer.WriteEndElement();
        }

        private static void WriteComets(XmlTextWriter writer, ConcurrentBag<Comet> comets)
        {
            writer.WriteStartElement("Comets");
            foreach (Comet c in comets)
            {
                writer.WriteStartElement("Comet");
                writer.WriteElementString("Diameter", c.Diameter.ToString());
                writer.WriteElementString("Mass", c.Mass.ToString());
                writer.WriteElementString("Density", c.Density.ToString());
                writer.WriteEndElement();
            }
            writer.WriteEndElement();
        }

        private static void WriteAsteroidBelt(XmlTextWriter writer, AsteroidBelt asteroidBelt)
        {
            writer.WriteStartElement("AsteroidBelt");
            foreach (Asteroid a in asteroidBelt.Asteroids)
            {
                writer.WriteStartElement("Asteroid");
                writer.WriteAttributeString("Type", Enum.GetName(typeof(AsteroidType), a.Type));
                writer.WriteElementString("Diameter", a.Diameter.ToString());
                writer.WriteElementString("Mass", a.Mass.ToString());
                writer.WriteElementString("Density", a.Density.ToString());
                writer.WriteElementString("Temperature", a.Temperature.ToString());
                writer.WriteEndElement();
            }
            writer.WriteEndElement();
        }

        public static void WriteUniverseFile(ConcurrentQueue<SolarSystem> solarSystems)
        {
            CreateSceneFolder();

            XmlTextWriter writer = new XmlTextWriter("Scene/Universe.xml", Encoding.Unicode);

            writer.Formatting = Formatting.Indented;
            writer.Indentation = 4;
            writer.WriteStartDocument();
            writer.WriteStartElement("SolarSystems");

            foreach (SolarSystem s in solarSystems)
            {
                writer.WriteStartElement("SolarSystem");
                writer.WriteAttributeString("ID", s.ID.ToString());
                writer.WriteElementString("File", "SolarSystem" + s.ID + ".xml");
                writer.WriteEndElement();
            }

            writer.WriteEndElement();
            writer.WriteEndDocument();
            writer.Close();
        }
    }
}
