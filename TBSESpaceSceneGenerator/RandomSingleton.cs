﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TBSESpaceSceneGenerator
{
   
    public class SeedNotSetException : Exception
    {
        public SeedNotSetException() : base("You must set the Seed with .SetSeed() before using RandomSingleton")
        {}
    }

    public class RandomSingleton
    {

        private static RandomSingleton _randomSingleton;

        private Random _random;

        public static RandomSingleton Instance()
        {
            if (_randomSingleton == null)
                _randomSingleton = new RandomSingleton();

            return _randomSingleton;
        }

        private RandomSingleton() {}

        public void SetSeed(int seed)
        {
            _random = new Random(seed);
        }

        public int Next()
        {
            lock (_random)
            {
                if (_random == null)
                    throw new SeedNotSetException();

                return _random.Next();
            }
        }

        public int Next(int maxValue)
        {
            lock (_random)
            {
                if (_random == null)
                    throw new SeedNotSetException();

                return _random.Next(maxValue);
            }
        }

        public int Next(int minValue, int maxValue)
        {
            lock (_random)
            {
                if (_random == null)
                    throw new SeedNotSetException();

                return _random.Next(minValue, maxValue);
            }
        }

        public double NextDouble()
        {
            lock (_random)
            {
                if (_random == null)
                    throw new SeedNotSetException();

                return _random.NextDouble();
            }
        }
                
        public double NextDouble(double maxValue)
        {
            return NextDouble(0, maxValue);
        }

        public double NextDouble(double minValue, double maxValue)
        {
            lock (_random)
            {
                if (_random == null)
                    throw new SeedNotSetException();

                return (_random.NextDouble() * (maxValue - minValue)) + minValue;
            }
        }

        public void NextByte(byte[] buffer)
        {
            lock (_random)
            {
                if (_random == null)
                    throw new SeedNotSetException();

                _random.NextBytes(buffer);
            }
        }
    }
}
