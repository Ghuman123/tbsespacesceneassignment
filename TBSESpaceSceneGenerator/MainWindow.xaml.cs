﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using TBSESpaceSceneGenerator.Generators;
using TBSESpaceSceneGenerator.Structures;
using TBSESpaceSceneGenerator.XML;

namespace TBSESpaceSceneGenerator
    {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
        {
        public MainWindow()
            {
            InitializeComponent();
            this.Title += " v" + App.Version;
            }

        private void btnGenerateSpaceScene_Click(object sender, RoutedEventArgs e)
            {
            int seed, solarSystemCount;
            try
                {
                seed = Convert.ToInt32(txtSeed.Text);
                solarSystemCount = Convert.ToInt32(txtSolarSystemCount.Text);
                }
            catch (Exception ex)
                {
                MessageBox.Show("Error parsing textbox values.\n" + ex.Message, "Parsing Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
                }

            Task t = Task.Factory.StartNew(() =>
            {
                Dispatcher.Invoke(new Action(() =>
                {
                    txtOutput.Text = "Generating Universe..." + Environment.NewLine;

                }));


            });



            ConcurrentQueue<SolarSystem> solarSystems = new UniverseGenerator().Generate(seed, solarSystemCount);
            Dispatcher.Invoke(new Action(() =>
            {
                txtOutput.Text += "Generated " + solarSystems.Count + " Solar Systems" + Environment.NewLine;
                if (chkOutputXML.IsChecked == true)
                    {
                    txtOutput.Text += "Generating XML..." + Environment.NewLine;
                    Task t2 = Task.Factory.StartNew(() =>
                    {
                        SceneWriter.WriteUniverseFile(solarSystems);
                        SceneWriter.WriteSolarSystemFiles(solarSystems);
                        Dispatcher.Invoke(new Action(() =>
                            {
                                txtOutput.Text += "Generated XML";
                            }));
                    });
                    }
            }));
            }
        }
    }

