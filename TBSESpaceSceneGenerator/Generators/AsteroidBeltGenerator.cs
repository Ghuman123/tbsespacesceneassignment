﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TBSESpaceSceneGenerator.Structures;

namespace TBSESpaceSceneGenerator.Generators
{
    class AsteroidBeltGenerator
    {
        public AsteroidBeltGenerator() { }

        public AsteroidBelt Generate()
        {
            RandomSingleton random = RandomSingleton.Instance();
            AsteroidBelt asteroidBelt = new AsteroidBelt();
            AsteroidGenerator asteroidGenerator = new AsteroidGenerator();

            int asteroidCount = random.Next(1000, 5000);

            for (int i = 0; i < asteroidCount; i++)
            {
                asteroidBelt.AddAsteroid(asteroidGenerator.Generate());
            }

            return asteroidBelt;
        }
    }
}
