﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TBSESpaceSceneGenerator.Structures;

namespace TBSESpaceSceneGenerator.Generators
{
    class UniverseGenerator
    {
        public ConcurrentQueue<SolarSystem> Generate(int seed, int solarSystemCount)
        {
            ConcurrentQueue<SolarSystem> solarSystems = new ConcurrentQueue<SolarSystem>();  
            //List<SolarSystem> solarSystems = new List<SolarSystem>(solarSystemCount);
            SolarSystem.ResetCount();

            RandomSingleton.Instance().SetSeed(seed);

            SolarSystemGenerator solarSystemGenerator = new SolarSystemGenerator();

            Parallel.For(0, solarSystemCount, (i) =>
            {
                solarSystems.Enqueue(solarSystemGenerator.Generate(i));
            });

            return solarSystems;
        }
    }
}
